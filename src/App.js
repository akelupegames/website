import React, { useEffect } from 'react';
import {
  Container,
  Typography,
  Box,
  createTheme,
  ThemeProvider,
} from '@mui/material';
import CssBaseline from '@mui/material/CssBaseline';

const theme = createTheme({
  palette: {
    mode: 'dark',
    background: {
      default: '#000',
    },
  },
});

const Footer = () => {
  return (
    <Box sx={{ py: 2, bgcolor: '#000', color: '#fff', textAlign: 'center' }}>
      <Typography variant="body2" color="inherit">
        &copy; 2021-2023 Akelupen. All rights reserved.
      </Typography>
    </Box>
  );
};

const App = () => {
  
  useEffect(() => {
    document.title = 'Akelupen: read, play, craft new realities';
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Box minHeight="100vh" display="flex" flexDirection="column">
        <Box flex={1}>
          <Container maxWidth="sm">
            <Box
              display="flex"
              flexDirection="column"
              alignItems="center"
              justifyContent="center"
              sx={{ height: '100%' }}
            >
              <Box sx={{ marginBottom: 0, marginTop: 10 }}>
                <img src="/logo.png" alt="Logo" className="App-logo" />
                <Typography variant="h1" align="center" gutterBottom>
                <h1>AKELUPEN</h1>
                </Typography>
                
              </Box>
              <Box sx={{ marginBottom: 3, marginTop: 0 }}>
                <Typography variant="h4" align="center" gutterBottom style={{ width: '100%', margin: '0 auto', display: 'block' }}>
                  Read. Play. <br></br>Craft new realities. 
                </Typography>
              </Box>
              <Typography variant="h5" align="center" gutterBottom>
                First release TBA 2023
              </Typography>
            </Box>
          </Container>
        </Box>
        <Footer />
      </Box>
    </ThemeProvider>
  );
};

export default App;
